# OpenML dataset: SWD

https://www.openml.org/d/1028

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

1. Title: Social Workers Decisions (Ordinal SWD)

2. Source Informaion:
Donor: Arie Ben David
MIS, Dept. of Technology Management
Holon Academic Inst. of Technology
52 Golomb St.
Holon 58102
Israel
abendav@hait.ac.il
Owner: Yoav Ganzah
Business Administration School
Tel Aviv Univerity
Ramat Aviv 69978
Israel

3. Past Usage:

4. Relevant Information
The SWD data set contains real-world assessments of qualified social workers
regarding the risk facing children if they stayed with their families at
home.  This evaluation of risk assessment is often presented to judicial
courts to help decide what is in the best interest of an alleged abused or
neglected child.


5. Number of Instances: 1000

6. Number of Attributes: 10 input, 1 output.

7. Attribute Information: All input and output values are ORDINAL.

8. Missing Attribute Values: None.

9. Class Distribution:

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1028) of an [OpenML dataset](https://www.openml.org/d/1028). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1028/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1028/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1028/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

